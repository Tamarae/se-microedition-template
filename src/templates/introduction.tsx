import React from "react"

import { Theme } from "@mui/material/styles";
import makeStyles from '@mui/styles/makeStyles';
import Container from "@mui/material/Container"
import Typography from "@mui/material/Typography"

import Layout from "../components/layout"
import { GatsbyImage, IGatsbyImageData } from "gatsby-plugin-image"
import SEO from "../components/seo";


declare module '@mui/styles/defaultTheme' {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface DefaultTheme extends Theme {}
}


interface Props {
  pageContext: {
    title?: string
    html: string
    issue: string
    site: Site
    orcid: {
      nodes: {
        childImageSharp: {
          gatsbyImageData: IGatsbyImageData
        }
      }[]
    }
  },
  location: any
}

const useStyles = makeStyles({
  orcid: {
    marginLeft: ".5rem"
  },
  Content: {
    "& p": {
      textIndent: "2rem"
    },
    "& p:first-of-type, & h2 + p, & h3 + p, & h4 + p, & h5 + p, & h6 + p, & .noindent p": {
      textIndent: "0"
    },
    "& blockquote": {
      marginLeft: "5rem"
    },
    "& .footnote-backref": {
      marginLeft: ".75rem"
    }
  }
})

export default function Introduction({pageContext}: Props) {
  const {site, orcid} = pageContext
  const classes = useStyles()

  const authors = site.siteMetadata.authors.map((a: Author) => (
    <React.Fragment key={a.last}>
      {a.first} {a.middle || ''} {a.last}, {a.affiliations.join(', ')}
      {a.orcid && 
        <a href={`https://orcid.org/${a.orcid}`} className={classes.orcid}>
          <GatsbyImage image={orcid.nodes[0].childImageSharp.gatsbyImageData} alt="ORCID logo"/>
        </a>
      }
      <br/>
    </React.Fragment>
  ))

  return (
    <Layout location="Introduction">
      <Container component="main" maxWidth="md">
        <Typography variant="h3" component="h2" gutterBottom={false} dangerouslySetInnerHTML={
          {__html: site.siteMetadata.htmlTitle}
        } />
        {pageContext.title ?
          <Typography variant="h4" component="h3" gutterBottom={false} dangerouslySetInnerHTML={
            {__html: pageContext.title}
          } />
        : ''}
        <Typography variant="h5" component="h4" gutterBottom={false} >
            Edited by {site.siteMetadata.authors.length > 1 ? <br/> : ''}
            {authors}
        </Typography>
        <Typography
          className={classes.Content}
          variant="body1"
          gutterBottom
          component="div"
          dangerouslySetInnerHTML={{ __html: pageContext.html }}
        />
      </Container>
    </Layout>
  );
}

export const Head = ({pageContext}: Props) => {
  const { site, slug } = pageContext
  const {htmlTitle, doi, authors, keywords, description, issue, issn} = site.siteMetadata
  const safeTitle = htmlTitle.replace(/<[^>]+>/g, '') || ""
  const fullTitle = `${safeTitle} | ${issue.short} | Scholarly Editing`
  const issueDateObj = new Date(issue.date)
  const fullPath = `http://scholarlyediting.org/issues/${issue.path}/${slug}`
  const formatAuthor = (a: Author) => (`${a.last}, ${a.first}${a.middle ? ` ${a.middle}` : '' }`)
  return (
  <SEO>
    <html lang="en" />
    <title>{fullTitle}</title>
    <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/" />
    <meta name="DC.type" content="Text" />
    <meta name="DC.format" content="text/html" />
    <meta name="DC.identifier" scheme="URI" content={fullPath} />
    <meta name="DC.identifier" scheme="ISSN" content={site.siteMetadata.issn} />
    <meta name="DC.title" content={safeTitle} />
    <meta name="DC.publisher" content="The Association for Documentary Editing" />
    <meta name="DC.language" scheme="RFC3066" content="en" />
    <meta name="DC.type" content="journalArticle" />
    <meta name="DC.identifier" scheme="DOI" content={doi} />
    <meta name="citation_doi" content={doi} />
    {authors.map(a => <>
      <meta name="author" content={formatAuthor(a)} />
      <meta name="DC.creator" content={formatAuthor(a)} />
    </>)}
    {keywords.length > 0 ? <>
      <meta name="keywords" content={keywords.join(", ")}/>
      <meta name="DC.subject" lang="en" content={keywords.join(", ")}/>
      <meta name="citation_keywords" content={keywords.join(", ")}/>
    </> : ""}
    <meta name="citation_abstract" lang="en" content={description} />
    <meta name="DC.date" scheme="W3CDTF" content={issue.date} />
    <meta name="DC.rights" content="
                For this publication a Creative Commons Attribution 4.0 International license has been granted by the author(s) who retain full copyright.
          " />
    <meta name="DC.relation.isPartOf" content={`Scholarly Editing Journal, ${issue}, ${issueDateObj.getFullYear()}`} />
    <meta name="DC.source" content="https://scholarlyediting.org" />
    <meta name="citation_journal_title" content="Scholarly Editing Journal" />
    <meta name="citation_publisher" content="The Association for Documentary Editing" />
    <meta name="citation_authors" content={authors.map(a => formatAuthor(a)).join("; ")} />
    <meta name="citation_title" content={safeTitle} />
    <meta name="citation_publication_date" content={issue.date.replace(/-/g, "/")} />
    <meta name="citation_online_date" content={issue.date.replace(/-/g, "/")} />
    <meta name="citation_issn" content={issn} />
    <meta name="citation_issue" content={issue.full} />
    <meta name="citation_language" content="en" />
    <meta name="citation_abstract_html_url" content={fullPath} />
    <meta name="citation_fulltext_html_url" content={fullPath} />
    <meta name="citation_pdf_url" content={`${fullPath}.pdf`} />
    <link title="schema(PRISM)" rel="schema.prism" href="http://prismstandard.org/namespaces/basic/2.0/"/>
    <meta name="prism.url" content={safeTitle}/>
    <meta name="prism.publicationName" content="Scholarly Editing Journal"/>
    <meta name="prism.number" content={issue.full}/>
    <meta name="prism.issueName" content={issue.full}/>
    <meta name="prism.publicationDate" content={`${issue.date}T00:00:00+02:00`}/>
    <meta name="prism.eIssn" content={issn}/>
    <meta property="og:type" content="article" />
    <meta property="og:url" content={fullPath} />
    <meta property="og:title" content={safeTitle} />
    <meta name="twitter:title" content={safeTitle}/>
    <meta name="twitter:card" content="summary" />
  </SEO>
)}
