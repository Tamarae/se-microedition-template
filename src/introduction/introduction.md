---
path: "/"
---

**This is a template for the Micro-Edition subsites for the _Scholarly Editing_ journal.** Typically this page would show the _Introduction_ to the Micro-Edition. In this case, this page provides some guidance over common Micro-Edition features included in this template. All the features are optional and should be pared down depending on the needs of each Micro-Edition.

## Facsimile Pages

...

## Footnotes

...

## Shared Context for Display Options

...

### Context for multilingual applications

...

## Sticky Micro-Edition AppBar

...

## Accessible Drop down menu with display options

...

## Table of Contents