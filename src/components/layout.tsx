import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import CssBaseline from "@mui/material/CssBaseline"
import { ThemeProvider, Theme, StyledEngineProvider } from "@mui/material/styles";

import theme from "../theme"
import Header from "./header"
import Footer from "./footer"
import EditionFooter from "./editionFooter"
import Button from "@mui/material/Button";
import Container from "@mui/material/Container";


declare module '@mui/styles/defaultTheme' {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface DefaultTheme extends Theme {}
}

type Children = JSX.Element | JSX.Element[]

interface Props {
  location?: string
  editionPage?: boolean
  children?: Children
}

// Style
interface Style {
  [key: string]: React.CSSProperties | Style
}

const styles: Style = {
  main: {
    paddingBottom: "1.45rem",
    minHeight: "60vh",
    "& h2, & h3": {
      paddingBottom: "1rem",
    },
  },
  skip: {
    position: 'absolute',
    top: '134px',
    padding: '12px',
    zIndex: '99',
    backgroundColor: '#dc3522',
    color: 'white !important',
    border: '2px solid white',
    left: '12px',
    clip: 'rect(0 0 0 0)',
    "&:focus": {
      clip: 'unset'
    }
  }
}

const Layout = ({ location, children, editionPage = false }: Props) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
          doi
          issue {
            short
            path
          }
          repository
          menuLinks {
            name
            link
          }
        }
      }
    }
  `)
  
  const {repository, title, menuLinks, doi, issue} = data.site.siteMetadata

  let footer = <Footer repository={repository}/>
  if (editionPage) {
    footer = <EditionFooter repository={repository}>{footer}</EditionFooter>
  }

  return (
    <div>
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <Button component="a" href="#pagecontent" sx={styles.skip}>Skip to content.</Button>
          <Header
            issue={issue}
            location={location || ''}
            siteTitle={title}
            menuLinks={menuLinks}
            doi={doi}
          />
          <Container component="main" maxWidth="md" sx={styles.main} id="pagecontent">
            {children}
          </Container>
          {footer}
        </ThemeProvider>
      </StyledEngineProvider>
    </div>
  );
}

export default Layout
