interface Author {
  first: string
  middle?: string
  last: string,
  affiliations: string[]
  orcid?: string,
}

interface Site {
  siteMetadata: {
    title: string
    htmlTitle: string
    issue: {
      full: string
      short: string
      path: string
      date: string
    }
    authors: Author[]
    issn: string
    doi: string
    keywords: string[]
    description: string
  }
}