function slugify(text) {
  return text.toLowerCase()
    .replace(/<[^>]+>/g, '') // remove html tags
    .replace(/ /g,'-') // spaces become -
    .replace(/-+/g, '-') // no repeated -
    .replace(/[^\w-]+/g,'') // remove all non word or - characters. TODO: this only saves ASCII characters!
}

exports.createPages = async ({ actions, graphql, reporter }) => {
  const { createPage } = actions
  await makeIntroduction(createPage, reporter, graphql)
  await makeCeteiceanPages(createPage, reporter, graphql)
}

async function makeCeteiceanPages(createPage, reporter, graphql) {
  const component = require.resolve(`./src/gatsby-theme-ceteicean/components/Ceteicean.tsx`)

  const result = await graphql(`
  query {
    allCetei {
      nodes {
        prefixed
        elements
        parent {
          ... on File {
            name
            ext
          }
        }
      }
    }
    site {
      siteMetadata {
        htmlTitle
        issue {
          short
        }
      }
    }
  }
`)
  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }
  for (const node of result.data.allCetei.nodes) {
    const {name, ext} = node.parent
    
    createPage({
      path: name,
      component,
      context: {
        site: result.data.site,
        name,
        prefixed: node.prefixed,
        elements: node.elements
      }
    })
  }
}

async function makeIntroduction(createPage, reporter, graphql) {
  const component = require.resolve(`./src/templates/introduction.tsx`)

  const result = await graphql(`
    query {
      allMarkdownRemark {
        edges {
          node {
            html
            frontmatter {
              path
              title
            }
          }
        }
      }
      site {
        siteMetadata {
          title
          htmlTitle
          issue {
            full
            short
            date
          }
          doi
          authors {
            first
            middle
            last
            affiliations
            orcid
          }
          issn
          keywords
          description
        }
      }
      orcid: allFile(filter: {relativePath: {eq: "orcid.png"}}) {
        nodes {
          childImageSharp {
            gatsbyImageData(width: 16)
          }
        }
      }
    }
  `)
  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }
  result.data.allMarkdownRemark.edges.forEach(({ node }) => {
    createPage({
      path: node.frontmatter.path,
      component,
      context: {
        site: result.data.site,
        slug: slugify(result.data.site.siteMetadata.title),
        doi: result.data.site.siteMetadata.doi,
        orcid: result.data.orcid,
        html: node.html,
        title: node.frontmatter.title
      }
    })   
  })
}
